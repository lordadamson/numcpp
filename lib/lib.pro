TEMPLATE = lib
CONFIG += staticlib c++14
QMAKE_CXXFLAGS += -fPIC
CONFIG -= qt
TARGET = numcpp

HEADERS += \
    numcpp_tensor.h

SOURCES += \
    numcpp_tensor.cpp
