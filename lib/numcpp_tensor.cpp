#include <numcpp_tensor.h>

#include <numeric>
#include <functional>
#include <algorithm>

using namespace Numcpp;

Tensor::Tensor(const Tensor& other) :
	data(other.data),
	dimensions(other.dimensions)
{
}

Tensor::Tensor(Tensor&& other) :
	data(std::move(other.data)),
	dimensions(std::move(other.dimensions))
{
}

Tensor& Tensor::operator =(const Tensor& other)
{
	data = other.data;
	dimensions = other.dimensions;
	return *this;
}

Tensor& Tensor::operator =(Tensor&& other)
{
	data = std::move(other.data);
	dimensions = std::move(other.dimensions);
	return *this;
}

Tensor::Tensor(const std::vector<size_t>& dimensions, Real initial_value)
{
	using std::begin;
	using std::end;

	// multiplies the values in dimensions
	auto product = std::accumulate(begin(dimensions), end(dimensions), 1, std::multiplies<size_t>());
	data = std::vector<Real>(product, initial_value);
}

Tensor Tensor::operator +(const Tensor& other)
{
	if(data.size() != other.data.size())
	{
		throw std::runtime_error("Cannot add Tensors that are not of the same size.");
	}

	std::vector<Real> result;
	std::transform (data.begin(), result.end(), other.data.begin(), result.begin(), std::plus<Real>());
	return Tensor(result, dimensions);
}

Tensor Tensor::operator *(const Tensor& other)
{

}

Tensor& Tensor::operator +=(const Tensor& other)
{
	if(data.size() != other.data.size())
	{
		throw std::runtime_error("Cannot add Tensors that are not of the same size.");
	}

	std::transform (data.begin(), data.end(), other.data.begin(), data.begin(), std::plus<Real>());
	return *this;
}

Tensor& Tensor::operator *=(const Tensor& other)
{

}
