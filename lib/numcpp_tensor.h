#ifndef NUMCPP_TENSOR_H
#define NUMCPP_TENSOR_H

#include <vector>
#include <cstddef>

namespace Numcpp {

#ifdef __NUMCPP_DOUBLE__
typedef double Real;
#else
typedef float Real;
#endif

struct Tensor
{
	std::vector<Real> data;
	std::vector<size_t> dimensions;

	Tensor(Tensor&& other);
	Tensor(const Tensor& other);
	Tensor& operator =(const Tensor& other);
	Tensor& operator =(Tensor&& other);

	template<typename T1, typename T2>
	Tensor(T1&& data, T2&& dimensions) :
		data(std::forward<T1>(data)), dimensions(std::forward<T2>(dimensions))
	{}

	Tensor(const std::vector<size_t>& dimensions, Real initial_value = 0);

	Tensor operator +(const Tensor& other);
	Tensor operator *(const Tensor& other);

	Tensor& operator +=(const Tensor& other);
	Tensor& operator *=(const Tensor& other);
};

} // namespace Numcpp

#endif // NUMCPP_TENSOR_H
